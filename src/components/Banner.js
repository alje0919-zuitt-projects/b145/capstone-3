import {Row, Col, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner() {

	return(
		<Row>
			<Col className="p-5">
				<h1>High-Fi Store</h1>
				<p>An online store that sells high fidelity equipment.</p>
				<Button variant="danger" as={Link} to="/products">Browse products</Button>
			</Col>

		</Row>


		)
}