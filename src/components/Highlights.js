import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights() {
	
	return (

		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>Fast Delivery</Card.Title>
						<Card.Text>
							Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ex dicta quia corrupti deleniti eius nemo dolor, recusandae perspiciatis ullam quos vel laboriosam culpa tempore numquam commodi, sapiente perferendis? Libero, officia.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>Only Best Brands</Card.Title>
						<Card.Text>
							Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ex dicta quia corrupti deleniti eius nemo dolor, recusandae perspiciatis ullam quos vel laboriosam culpa tempore numquam commodi, sapiente perferendis? Libero, officia.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>Payment Secure System</Card.Title>
						<Card.Text>
							Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ex dicta quia corrupti deleniti eius nemo dolor, recusandae perspiciatis ullam quos vel laboriosam culpa tempore numquam commodi, sapiente perferendis? Libero, officia.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

		)
}