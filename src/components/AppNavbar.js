import { Fragment, useContext, useState, useEffect } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
// to make the menu links work
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

// as={} to make it clickable and routes to the desired URL
// to="" put the url path
// <Nav.Link as={Link} to..> [is equivalent to] <Link className="nav-link">

export default function AppNavbar() {

	const { user } = useContext(UserContext);
	/*
		syntax:
			localStorage.getItem(propertyName)
	*/
	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user)
	
	

	return(
		<Navbar bg="light" expand="lg">
		  <Container>
		    <Navbar.Brand as={Link} to="/">High-Fi</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="ml-auto">
		        {/*<Nav.Link as={Link} to="/">Home</Nav.Link>*/}
		        <Nav.Link as={Link} to="/products">Products</Nav.Link>
		  		
		  		{ 
		  			(user.id !== null) ?
		  			<Fragment>
		  				<Nav.Link as={Link} to="/userdetails">My Account</Nav.Link>
		  				<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
		  			</Fragment>
		  			:
		  			<Fragment>
		  				<Nav.Link as={Link} to="/register">Register</Nav.Link>
		  				<Nav.Link as={Link} to="/login">Login</Nav.Link>
		  			</Fragment>
		  		}
		        
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
		)
}