import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';

import App from './App'; // AppNavbar will be placed to App
// import navbar
  // import AppNavbar from './components/AppNavbar';

// import the bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

