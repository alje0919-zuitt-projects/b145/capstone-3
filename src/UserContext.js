import React from 'react';

// Create a Context Object
// a context object as the name state, is a data type of an object that can be used to store information that can be shared to other components within the app.
// the context object is a different approach to passing information between components and allows easier access by avoiding the use of a prop-drilling.

// React.createContext();

const UserContext = React.createContext();

// The 'Provider' component allows other components to consume/use the context object and supply the necessary information needed to the context object.

// UserContext.Provider;

export const UserProvider = UserContext.Provider;

export default UserContext;