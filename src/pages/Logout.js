import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';
// redirect on old version

export default function Logout() {

	const { unsetUser, setUser } = useContext(UserContext);
	// Clear the localStorage
	unsetUser()

	// by adding the useEffect, this will allow the logout page to render first before
	useEffect(() => {
		// set the user state back into it's original value
		setUser({id: null});
	}, [])

	return(

		<Navigate to="/" />

		)

}