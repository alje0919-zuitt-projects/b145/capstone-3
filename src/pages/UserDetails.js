import { useState, useEffect, useContext } from "react";
// import UserOrders from "../components/UserOrders";
import { Container, Button } from "react-bootstrap";
import UserContext from "../UserContext";
import { Link } from "react-router-dom";

export default function UserDetails() {

  const [ userFN, setUserFN ] = useState("");
  const [ userLN, setUserLN ] = useState("");
  const [ userOrders, setUserOrders ] = useState([]);

  const token = localStorage.getItem("token");
  const {user, setUser} = useContext(UserContext);

  useEffect(() => {
      fetch("https://zuitt-b164-capstone3-alje.herokuapp.com/api/users/details", {

        headers: {
          Authorization: `Bearer ${ token }`
      }
      })
      .then(res => res.json())
      .then(data => {

		setUserFN(data.firstName)
      	setUserLN(data.lastName)
      	setUserOrders(data.orders)

      	// setUserOrders(data.map(userOrders => {
      	// 	return(
      	// 		<UserOrders key={userOrders._id} ordersProp={userOrders}/>
      	// 		)
      	// }))

		  //console.log(data)
          // setUsers(data.map(users => {
          //     return(
          //         <UserdetailsCard key={users._id} userProp={users}/>
          //     )
          // }))
      })
  })
return (
  
    (user.id !== null) ?
    <Container>
    	<h1 className="text-center my-3">User Details</h1>
    	<p>First Name: <strong>{userFN}</strong></p>
    	<p>Last Name: <strong>{userLN}</strong></p>

    	<p>Orders: {setUserOrders}</p>
    	
    	
    </Container>
    : 
    <Container className="my-5 text-center">
    	<h1>Please Log in to view order</h1>
    <Button variant="info" as={Link} to={"/login"}>Sign in</Button>
    
  </Container>
)
}
