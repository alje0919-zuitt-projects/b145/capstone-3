import { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView() {

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	// useParams hook allows us to retrieve the productId passed via the URL
	const { productId } = useParams()

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const order = (productId) => {
		fetch("https://zuitt-b164-capstone3-alje.herokuapp.com/api/users/order", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data) //true //false error (maybe user is admin)
			if (data === true) {
				Swal.fire({
					title: "Successfully Ordered",
					icon: "success",
					text: "You have successfully ordered this product."
				})
				navigate("/products")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please check user role"
				})
			}
		})
	}

	useEffect(() => {
		console.log(productId)
		fetch(`https://zuitt-b164-capstone3-alje.herokuapp.com/api/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	}, [productId])

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							{/*<Card.Subtitle>Description:</Card.Subtitle>*/}
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price: {price}</Card.Subtitle>
							<Card.Text></Card.Text>
							{/*<Card.Subtitle>Class Schedule:</Card.Subtitle>
							<Card.Text>5:30PM-9:30PM</Card.Text>*/}
							{
								user.id !== null ?
								<Button variant="danger" onClick={() => order(productId)}>Place order</Button>
								:
								<Link className="btn btn-danger" to="/login">Log in to Order</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>

		</Container>
		)

}