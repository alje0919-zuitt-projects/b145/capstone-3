import {Row, Col, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function ErrorPage() {

	return(
		<Row>
			<Col className="p-5">
				<h1>Error 404</h1>
				<p>The page you are trying to access cannot be found.</p>
				<Button variant="danger" as={Link} to="/">Go to homepage</Button>
			</Col>

		</Row>


		)
}


