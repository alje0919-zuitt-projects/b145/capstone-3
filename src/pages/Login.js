import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login() {

	// 'useContext' hook is used to deconstruct/unpack the data of the UserContext object.
	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	// State to determine whethter the button
	const [isActive, setIsActive] = useState(true);

	// console.log(email);
	// console.log(password);
	// console.log(password2);

	function authenticate(e){
		// prevents page redirection via a form submission
		e.preventDefault()

		
		/*syntax:
			fetch("URL", {options})
			.then(res => res.json)
			.then(data => {})*/
		
		fetch('https://zuitt-b164-capstone3-alje.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
				// 'Accept': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			// console.log(data)
			if(typeof data.accessToken !== "undefined") {
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken)

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to High-Fi"
				});
			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check login credentials and retry"
				})
			}
		})


		// clear input fields after clicking submit
		setEmail("");
		setPassword("");


		/*
		syntax:
			localStorage.setItem()
			store the email inside the localStorage
		*/

		//localStorage.setItem("email", email);

		// set the global user state to have properties from local storage
		// setUser({
			// getItem() getting the email inside the localStorage
			// email: localStorage.getItem('email')
		//})

		// alert(`${email} has been verified. Welcome back!`)
	}

	const retrieveUserDetails = (token) => {
		fetch('https://zuitt-b164-capstone3-alje.herokuapp.com/api/users/details/', {
			headers: {
				Authorization: `Bearer ${ token }`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}


	useEffect(() => {
		// validation to enable the Submit button when all the input fields are populated and both passwords match.
		if(email !== "" && password !== "") {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])
	
	return(

		(user.id !== null) ?
		<Navigate to="/products" /> //routes to a page after pressing a button (e.g. login)
		:

		<Form onSubmit={(e) => authenticate(e)}>
		<h1 className="text-center">Login Page</h1>
		  <Form.Group
		  		className="mb-3"
		  		controlId="userEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control
		    	autoComplete="off"
		    	type="email"
		    	placeholder="Enter email"
		    	value={email}
		    	onChange={e => 
		    		setEmail(e.target.value)
		    		// console.log(e.target.value)
		    	}
		    	required />
		    
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="password">
		    <Form.Label>Password</Form.Label>
		    <Form.Control
		    	type="password"
		    	placeholder="Password"
		    	value={password}
		    	onChange={e => setPassword(e.target.value)}
		    	required />
		  </Form.Group>

		  {
		  	isActive ?
		  	<Button variant="danger" type="submit" id="submitBtn">
		  	  Submit
		  	</Button>
		  	:
		  	<Button variant="secondary" type="submit" id="submitBtn" disabled>
		  	  Submit
		  	</Button>
		  }
		</Form>


		)
}