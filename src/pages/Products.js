import { Fragment, useEffect, useState, useContext } from 'react';
// import ProductCard from '../components/ProductCard';
import UserView from "../components/UserView";
import AdminView from "../components/AdminView";
import UserContext from "../UserContext";

import { Row, Col, Card } from 'react-bootstrap';
// import coursesData from '../data/coursesData';


export default function Products() {

	const { user } = useContext(UserContext);

	const [productsData, setProductsData] = useState([])

	// console.log(courseData)

	const fetchData = () => {
		fetch(`https://zuitt-b164-capstone3-alje.herokuapp.com/api/products`)
		.then(res => res.json())
		.then(data => {
			//.then has what is called a "self-contained scope"

			//Any code inside of this .then only exists inside of this .then, and therefore cannot be processed properly by React

			//to solve this problem, we use a state. By setting the new value of our state to be the data from our server, that state can be seen by our entire component
			setProductsData(data)
		})
	}

	//on component mount/page load, useEffect will run and call our fetchData function, which runs our fetch request
	useEffect(() => {
		fetchData()
	}, [])

	return(user.isAdmin ?
		<AdminView productsProp={productsData} fetchData={fetchData}/>
		:
		<UserView productsProp={productsData}/>)
}










// export default function Products() {


// 	const [product, setProducts] = useState([])
	

// 	useEffect(() => {
// 		fetch('http://localhost:4000/api/products/')
// 		.then(res => res.json())
// 		.then(data => {
// 			console.log(data)

// 			setProducts(data.map(product => {
// 				return(
// 					<ProductCard key={product._id} productProp={product} />
// 					)
// 			}))
// 		})
// 	}, [])

// 	return(
// 		<Fragment >
// 			<h1 className="mt-3 mb-3">Products</h1>
// 			{/*<CourseCard courseProp={coursesData[0]} />*/}
// 					{product}
// 		</Fragment>
// 		)
// }